import { useContext, createContext } from "react";

const AppContext = createContext(null);
export const AppContextProvider = AppContext.Provider;

export function useAppContext() {
  return useContext(AppContext);
}