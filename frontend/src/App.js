import './App.css';
import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Header from './components/header/Header';
import LoginPage from './pages/auth/LoginPage';
import RegisterPage from './pages/auth/RegisterPage';
import ForgotPassword from './pages/auth/ForgotPassword';
import CampaignPage from './pages/Campaign/Campaign';
import CampaignAboutPage from './pages/Campaign/CampaignAbout';
import CampaignWeGoToThemPage from './pages/Campaign/CampaignWeGoToThemPage';
import UserPage from './pages/User/User';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { CssBaseline } from '@mui/material';
import { Auth, API } from "aws-amplify";
import { AppContextProvider } from "./util/context";

const defaultTheme = {
  mode: 'light',
  primary: {
    main: '#00b6f1',
  },
  secondary: {
    main: '#ee4790',
  },
};

export default function App() {
  const [isAuthenticating, setIsAuthenticating] = useState(true);
  const [user, setUser] = useState(null);
  const [token, setToken] = useState(null);
  const [isDark, setIsDark] = useState(window.localStorage.getItem('isDark') === 'true');
  const [theme, setTheme] = useState(createTheme({ palette: defaultTheme }));

  useEffect(() => {
    loadUser();
    setIsAuthenticating(false);
  }, []);

  useEffect(() => {
    window.localStorage.setItem('isDark', isDark);
    setTheme(createTheme({ palette: {
      ...defaultTheme,
      mode: isDark ? 'dark' : 'light'
    }}));
  }, [isDark]);

  async function loadUser() {
    try {
      const authenticatedUser = await Auth.currentAuthenticatedUser();
      setToken(`Bearer ${authenticatedUser.signInUserSession.getIdToken().getJwtToken()}`);
      const userData = await API.get('api', `/user/${authenticatedUser.username}?simple=true`);
      setUser({
        id: authenticatedUser.username,
        name: userData.name,
        avatar: userData.avatar
      });
      return authenticatedUser.username;
    } catch (error) {
      if (error !== 'The user is not authenticated') {
        console.log(error);
      }
    }
  }

  function logout() {
    setUser(null);
  }

  return (
    !isAuthenticating && (
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <BrowserRouter>
          <AppContextProvider value={{ user, token, loadUser, logout, isDark, setIsDark }}>
            <Header />
            <Routes>
              <Route exact path='/' element={<CampaignPage campaignId='london-to-tehran' />} />
              <Route path='/login' element={<LoginPage />} />
              <Route path='/register' element={<RegisterPage />} />
              <Route path='/forgot' element={<ForgotPassword />} />
              <Route path='/campaign/:id' element={<CampaignPage />} />
              <Route path='/campaign/:id/about' element={<CampaignAboutPage />} />
              <Route path='/campaign/:id/wegotothem' element={<CampaignWeGoToThemPage />} />
              <Route path='/user/:id' element={<UserPage />} />
            </Routes>
          </AppContextProvider>
        </BrowserRouter>
      </ThemeProvider>
    )
  );
}