import { Alert, Box, Button, Card, CardActions, CardContent, CardMedia, Grid, Link, TextField, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import GroupIcon from '@mui/icons-material/Group';
import React, { useState } from 'react';
import { API } from "aws-amplify";
import { useAppContext } from "../../util/context";

export default function CampaignCard(props) {
  const [bio, setBio] = useState(props.bio);
  const [tempBio, setTempBio] = useState(props.bio);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [validationError, setValidationError] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const { user, token } = useAppContext();

  function reset() {
    setTempBio(bio);
    setIsEditing(false);
    setValidationError(null);
  }

  async function submit() {
    if (isSubmitting) {
      return;
    }
    setValidationError(null);
    setIsSubmitting(true);
    try {
      await API.put('api', `/campaign/${props.id}`, {
        headers: {
          Authorization: token,
          'Content-Type': 'application/json'
        },
        body: { bio: tempBio }
      });
      setBio(tempBio);
      setIsEditing(false);
    } catch (error) {
      console.log(error.response);
      setValidationError(error.response?.data?.error || 'There was a problem submitting your request');
    } finally {
      setIsSubmitting(false);
    }
  }

  return (
    <Card sx={{ maxWidth: 1000 }}>
      <Grid container justifyContent='center' alignItems='center' sx={{ backgroundColor: '#fff200' }}>
        <Grid item sm={6}>
          <CardMedia
            component='img'
            image={ process.env.PUBLIC_URL + '/london-to-tehran.png' }
          />
        </Grid>
        <Grid item sm={6}>
          <CardMedia
            component='img'
            image={ process.env.PUBLIC_URL + '/in-support.png' }
          />
        </Grid>
      </Grid>
      { isEditing ?
        <TextField
          value={tempBio}
          onInput={ event => setTempBio(event.target.value) }
          label='Bio'
          disabled={isSubmitting}
          multiline
          rows={4}
          fullWidth
        />
      :
        <CardContent>
          {bio}
        </CardContent>
      }
      { isEditing ?
        <>
          { validationError &&
            <Alert severity="info">{ validationError }</Alert>
          }
          <CardActions sx={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button disabled={isSubmitting} onClick={ reset }>Cancel</Button>
            <LoadingButton variant='contained' onClick={submit} loading={isSubmitting}>Submit</LoadingButton>
          </CardActions>
        </>
      :
        <CardActions>
          <Button component={Link} href='campaign/london-to-tehran/about'>Morad & Mehran</Button>
          <Button component={Link} href='campaign/london-to-tehran/wegotothem' color='secondary'>If they can't come to us...</Button>
          { props.id === user?.id &&
            <Button onClick={ () => setIsEditing(true) }>Edit</Button>
          }
          <Box sx={{ marginLeft: 'auto', marginRight: 'auto' }} />
          <Typography sx={{ padding: 1 }}>
              <GroupIcon /> {props.users?.length || 0}
          </Typography>
        </CardActions>
      }
    </Card>
  );
}