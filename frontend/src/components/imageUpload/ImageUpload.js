import { Box, Button, CardMedia } from '@mui/material';
import React, { useState, useEffect } from 'react';

const DEFAULT_SIZE = 200;

export default function ImageUpload(props) {
  const [image, setImage] = useState(props.image || props.placeholder);

  useEffect(() => {
    if (props.image === '') {
      setImage('');
    }
  }, [props.image]);

  function onImageChange(event) {
    const newImage = event?.target?.files?.item(0);
    if (newImage) {
      setImage(URL.createObjectURL(newImage));
      props.setImage(newImage);
    }
  }

  return (
    <Box sx={{
      position: 'relative',
      maxWidth: props.width || DEFAULT_SIZE,
      maxHeight: props.height || DEFAULT_SIZE,
      minHeight: 30
    }}>
      <Button variant='contained' component='label' sx={{ position: 'absolute', left: '50%', transform: 'translate(-50%, 0%)', whiteSpace: 'nowrap' }}>
        Upload Image
        <input hidden type='file' accept='image/png, image/gif, image/jpeg' onChange={ onImageChange } />
      </Button>
      { image &&
        <CardMedia component='img' image={ image } sx={{ width: '100%', height: '100%' }} />
      }
    </Box>
  );
}