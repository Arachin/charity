import { Alert, Button, Card, CardActions, CardContent, CardHeader, InputAdornment, TextField } from '@mui/material';
import { LoadingButton } from '@mui/lab'
import React, { useState } from 'react';
import { API } from "aws-amplify";
import { useAppContext } from "../../../util/context";
import ImageUpload from '../../imageUpload/ImageUpload';

export default function SubmitFeed(props) {
  const [message, setMessage] = useState();
  const [distance, setDistance] = useState(0);
  const [image, setImage] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [validationError, setValidationError] = useState(null);
  const { token } = useAppContext();

  function reset() {
    setMessage('');
    setDistance('');
    setImage('');
    setValidationError(null);
  }

  async function uploadFile(image) {
    const response = await API.post('api', '/upload', {
      headers: {
        Authorization: token,
        'Content-Type': 'application/json'
      },
      body: { contentType: image.type }
    });
    await fetch(response.preSignedUrl, {
      method: 'PUT',
      headers: { 'Content-Type': image.type },
      body: image
    });
    return response.targetUrl;
  }

  async function submit() {
    if (isSubmitting) {
      return;
    }
    setValidationError(null);
    setIsSubmitting(true);
    try {
      let tempImage = '';
      if (image) {
        tempImage = await uploadFile(image);
      }
      await API.post('api', '/feed', {
        headers: {
          Authorization: token,
          'Content-Type': 'application/json'
        },
        body: {
          message,
          distance,
          image: tempImage
        }
      });
      props.updatePage();
      reset();
    } catch (error) {
      console.log(error.response);
      setValidationError(error.response?.data?.error || 'There was a problem submitting your request');
    } finally {
      setIsSubmitting(false);
    }
  }

  return (
    <Card>
      <CardHeader title='Submit Feed' />
      <CardContent>
        <TextField
          value={message}
          onInput={ event => setMessage(event.target.value) }
          label='Message'
          disabled={isSubmitting}
          multiline
          rows={4}
          fullWidth />
        <TextField
          value={distance}
          onInput={ event => setDistance(event.target.value) }
          label='Distance'
          type='number'
          right='true'
          InputProps={{ endAdornment: <InputAdornment position="end">mi</InputAdornment> }}
          inputProps={{
            min: 0,
            style: { textAlign: 'end' }
          }}
          disabled={isSubmitting}
        />
        <ImageUpload image={ image } setImage={ setImage } />
      </CardContent>
      { validationError &&
        <Alert severity="info">{ validationError }</Alert>
      }
      <CardActions sx={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button onClick={reset} disabled={isSubmitting}>Reset</Button>
        <LoadingButton variant='contained' onClick={submit} loading={isSubmitting}>Submit</LoadingButton>
      </CardActions>
    </Card>
  );
}