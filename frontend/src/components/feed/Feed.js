import React, { useRef, useEffect } from 'react';
import { Stack } from '@mui/material';
import { useLocation } from 'react-router-dom';
import FeedEntry from './feedEntry/FeedEntry';
import SubmitFeed from './submitFeed/SubmitFeed';
import { useAppContext } from "../../util/context";

export default function Feed(props) {
  const { user } = useAppContext();
  const timestamp = new URLSearchParams(useLocation().search).get('timestamp');
  const feedRefs = useRef([]);

  useEffect(() => {
    if (timestamp) {
      feedRefs.current[timestamp]?.scrollIntoView()
    }
  }, [timestamp]);

  return (
    <Stack spacing={2}>
        { props.owner === user?.id &&
          <SubmitFeed updatePage={props.updatePage} />
        }
        { props.feeds.map((feed) => (
          <FeedEntry {...feed} updatePage={props.updatePage} showUserDetails={props.showUserDetails} key={feed.timestamp} innerRef={(element) => (feedRefs.current[feed.timestamp] = element)} />
        ))}
    </Stack>
  );
}