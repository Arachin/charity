import { Alert, Avatar, Box, Button, Card, CardActions, CardContent, CardMedia, CardHeader, IconButton, Typography, TextField, InputAdornment } from '@mui/material';
import { LoadingButton } from '@mui/lab'
import FavoriteIcon from '@mui/icons-material/Favorite';
import DirectionsRunIcon from '@mui/icons-material/DirectionsRun';
import React, { useState } from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { API } from "aws-amplify";
import { FacebookShareButton, FacebookIcon, TwitterShareButton, TwitterIcon } from 'react-share';
import { useAppContext } from "../../../util/context";
import ImageUpload from '../../imageUpload/ImageUpload';

export default function FeedEntry(props) {
  const [message, setMessage] = useState(props.message);
  const [distance, setDistance] = useState(props.distance);
  const [image, setImage] = useState(props.image);
  const [tempMessage, setTempMessage] = useState(message);
  const [tempDistance, setTempDistance] = useState(distance);
  const [tempImage, setTempImage] = useState('');
  const [likes, setLikes] = useState(props.likes || []);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [validationError, setValidationError] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const { user, token } = useAppContext();

  function reset() {
    setTempMessage(message);
    setTempDistance(distance);
    setTempImage('');
    setIsEditing(false);
    setValidationError(null);
  }
  
  async function uploadFile(image) {
    const response = await API.post('api', '/upload', {
      headers: {
        Authorization: token,
        'Content-Type': 'application/json'
      },
      body: { contentType: image.type }
    });
    await fetch(response.preSignedUrl, {
      method: 'PUT',
      headers: { 'Content-Type': image.type },
      body: image
    });
    return response.targetUrl;
  }

  async function submit() { // TODO: Share code with the SubmitFeed
    if (isSubmitting) {
      return;
    }
    setValidationError(null);
    setIsSubmitting(true);
    try {
      let currentImage = image;
      if (tempImage) {
        currentImage = await uploadFile(tempImage);
      }
      await API.put('api', `/feed/${props.timestamp}`, {
        headers: {
          Authorization: token,
          'Content-Type': 'application/json'
        },
        body: {
          message: tempMessage,
          distance: tempDistance,
          image: currentImage
        }
      });
      setMessage(tempMessage);
      setDistance(tempDistance);
      setImage(currentImage);
      setTempImage('');
      setIsEditing(false);
    } catch (error) {
      console.log(error.response);
      setValidationError(error.response?.data?.error || 'There was a problem submitting your request');
    } finally {
      setIsSubmitting(false);
    }
  }

  async function deleteFeedEntry() {
    try {
      await API.del('api', `/feed/${props.timestamp}`, {
        headers: {  Authorization: token }
      });
      props.updatePage();
    } catch (error) {
      console.log(error);
    }
  }

  function getShareUrl() {
    let hostUrl = window.location.host;
    if (hostUrl.includes('localhost')){
      hostUrl = 'example.com';
    }
    return `http://${hostUrl}/user/${props.owner}?timestamp=${props.timestamp}`
  }

  function getFacebookMessage() {
    return message;
  }

  function getTwitterMessage() {
    const allowedLength = 255;
    if (message?.length > allowedLength) {
      return `${message.slice(0, allowedLength - 2)}…`;
    }
    return message;
  }

  function toggleLike() {
    try {
      if (likes.includes(user.id)) {
        API.del('api', `/feed/${props.timestamp}/${props.owner}/like`, {
          headers: {  Authorization: token }
        });
        setLikes(likes.filter(item => item !== user.id));
      } else {
        API.post('api', `/feed/${props.timestamp}/${props.owner}/like`, {
          headers: { Authorization: token }
        });
        setLikes(likes.concat(user.id));
      }
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <Card ref={props.innerRef}>
      { props.showUserDetails &&
        <CardHeader
          avatar={
            <Avatar src={props.avatar || process.env.PUBLIC_URL + '/defaultAvatar.png'} aria-label="user" />
          }
          component={Link} to={`/user/${props.owner}`} 
          title={props.name || props.owner}
          subheader={moment(props.timestamp).fromNow()}
          sx={{ padding: 2, color: 'text.primary', textDecoration: 'none' }}
        />
      }
      { isEditing ?
        <CardContent>
          <TextField
            value={tempMessage}
            onInput={ event => setTempMessage(event.target.value) }
            label='Message'
            disabled={isSubmitting}
            multiline
            rows={4}
            fullWidth
          />
          <TextField
            value={tempDistance}
            onInput={ event => setTempDistance(event.target.value) }
            label='Distance'
            type='number'
            right='true'
            InputProps={{ endAdornment: <InputAdornment position="end">mi</InputAdornment> }}
            inputProps={{
              min: 0,
              style: { textAlign: 'end' }
            }}
            disabled={isSubmitting}
          />
          <ImageUpload image={ image } setImage={ setTempImage } />
        </CardContent>
      :
        <>
          { message &&
            <CardContent sx={{ maxWidth: 600 }}>
              { message }
            </CardContent>
          }
          { image &&
            <CardMedia
              component="img"
              image={ image }
              sx={{ maxWidth: 600 }}
            />
          }
        </>
      }
      { isEditing ?
        <>
          { validationError &&
            <Alert severity="info">{ validationError }</Alert>
          }
          <CardActions sx={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button disabled={isSubmitting} onClick={ reset }>Cancel</Button>
            <LoadingButton variant='contained' onClick={submit} loading={isSubmitting}>Submit</LoadingButton>
          </CardActions>
        </>
      :
        <CardActions>
          { props.owner === user?.id &&
            <>
              <Button onClick={ () => setIsEditing(true) }>Edit</Button>
              <Button onClick={ deleteFeedEntry }>Delete</Button>
            </>
          }
          { distance > 0 &&
            <Typography sx={{ padding: 1 }}>
              <DirectionsRunIcon /> { distance } mi
            </Typography>
          }
          <Box sx={{ marginLeft: 'auto', marginRight: 'auto' }} />
          { user ?
            <>
              <FacebookShareButton
                url={getShareUrl()}
                quote={getFacebookMessage()}
                className="Demo__some-network__share-button"
              >
                <FacebookIcon size={32} round />
              </FacebookShareButton>
              <TwitterShareButton
                url={getShareUrl()}
                title={getTwitterMessage()}
                className="Demo__some-network__share-button"
              >
                <TwitterIcon  size={32} round />
              </TwitterShareButton>
              <IconButton
                aria-label="like"
                color={likes.includes(user.id) ? 'secondary' : 'default'}
                onClick={ toggleLike }
                sx={{ padding: 1 }}
              >
                { likes.length }<FavoriteIcon />
              </IconButton>
            </>
          :
            <Typography sx={{ padding: 1 }}>
              { likes.length }<FavoriteIcon />
            </Typography>
          }
        </CardActions>
      }
    </Card>
  );
}