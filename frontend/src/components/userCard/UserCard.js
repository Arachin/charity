import { Alert, Box, Button, Card, CardActions, CardContent, CardMedia, Typography, TextField, Link, InputAdornment } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import React, { useState } from 'react';
import { API } from "aws-amplify";
import { useAppContext } from "../../util/context";
import ImageUpload from '../imageUpload/ImageUpload';

export default function UserCard(props) {
  const [name, setName] = useState(props.name);
  const [bio, setBio] = useState(props.bio);
  const [donate, setDonate] = useState(props.donate);
  const [avatar, setAvatar] = useState(props.avatar);
  const [tempName, setTempName] = useState(props.name);
  const [tempBio, setTempBio] = useState(props.bio);
  const [tempDonate, setTempDonate] = useState(props.donate);
  const [tempTarget, setTempTarget] = useState(props.target);
  const [tempImage, setTempImage] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [validationError, setValidationError] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const { user, token, loadUser } = useAppContext();

  function simplifyUrl(url) {
    return url.split('://')[1] || url;
  }

  function reset() {
    setTempName(name);
    setTempBio(bio);
    setTempImage('');
    setIsEditing(false);
    setValidationError(null);
  }

  async function uploadFile(image) {
    const response = await API.post('api', '/upload', {
      headers: {
        Authorization: token,
        'Content-Type': 'application/json'
      },
      body: { contentType: image.type }
    });
    await fetch(response.preSignedUrl, {
      method: 'PUT',
      headers: { 'Content-Type': image.type },
      body: image
    });
    return response.targetUrl;
  }
  
  async function submit() {
    if (isSubmitting) {
      return;
    }
    setValidationError(null);
    setIsSubmitting(true);
    try {
      let tempAvatar = avatar;
      if (tempImage) {
        tempAvatar = await uploadFile(tempImage);
      }
      const userData = {
        name: tempName,
        bio: tempBio,
        donate: tempDonate,
        target: tempTarget,
        avatar: tempAvatar
      };
      await API.put('api', `/user/${props.id}`, {
        headers: {
          Authorization: token,
          'Content-Type': 'application/json'
        },
        body: userData
      });
      loadUser();
      props.updatePage();
      setName(tempName);
      setBio(tempBio);
      setDonate(tempDonate);
      setAvatar(tempAvatar);
      setTempImage('');
      setIsEditing(false);
    } catch (error) {
      console.log(error.response);
      setValidationError(error.response?.data?.error || 'There was a problem submitting your request');
    } finally {
      setIsSubmitting(false);
    }
  }

  return (
    <Card sx={{
      width: '100%',
      maxWidth: '750px'
    }}>
      <Box sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 2
      }}>
        { isEditing ?
          <ImageUpload
            image={ avatar }
            placeholder={ process.env.PUBLIC_URL + '/defaultAvatar.png' }
            setImage={ setTempImage }
          />
        :
          <CardMedia
            image={ avatar || process.env.PUBLIC_URL + '/defaultAvatar.png' }
            alt="user"
            sx={{
              width: 200,
              height: 200,
              objectFit: 'cover',
              borderRadius: '50%',
              flexShrink: 0
            }}
          />
        }
        { isEditing ?
          <TextField
            value={tempName}
            onInput={ event => setTempName(event.target.value) }
            label='Name'
            disabled={isSubmitting}
            fullWidth
          />
        :
          <CardContent>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              {name || props.id}
            </Typography>
          </CardContent>
        }
      </Box>
      { isEditing ?
        <>
          <TextField
            value={tempBio}
            onInput={ event => setTempBio(event.target.value) }
            label='Bio'
            disabled={isSubmitting}
            multiline
            rows={4}
            fullWidth
          />
          <TextField
            value={tempDonate}
            onInput={ (event) => {
              let url = event.target.value;
              if (url && !url.includes('http')) {
                url = `https://${url}`;
              }
              setTempDonate(url);
            }}
            label='Donate Link'
            disabled={isSubmitting}
            fullWidth
          />
          <TextField
            value={tempTarget}
            onInput={ event => setTempTarget(event.target.value) }
            label='Target Distance'
            type='number'
            right='true'
            InputProps={{ endAdornment: <InputAdornment position="end">mi</InputAdornment> }}
            inputProps={{
              min: 0,
              style: { textAlign: 'end' }
            }}
            disabled={isSubmitting}
            fullWidth
          />
        </>
      :
        <CardContent>
          {bio}
        </CardContent>
      }
      { isEditing ?
        <>
          { validationError &&
            <Alert severity="info">{ validationError }</Alert>
          }
          <CardActions sx={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button disabled={isSubmitting} onClick={ reset }>Cancel</Button>
            <LoadingButton variant='contained' onClick={submit} loading={isSubmitting}>Submit</LoadingButton>
          </CardActions>
        </>
      :
        <CardActions>
          { props.id === user?.id &&
            <Button onClick={ () => setIsEditing(true) }>Edit</Button>
          }
          <Box sx={{ marginLeft: 'auto', marginRight: 'auto' }} />
          { donate &&
            <>
              <Typography variant="button">Donation link:</Typography>
              <Button component={Link} href={donate}>{ simplifyUrl(donate) }</Button>
            </>
          }
        </CardActions>
      }
    </Card>
  );
}