import { Container, Slider } from '@mui/material';
import React from 'react';

export default function ProgressBar(props) {
  const target = Math.max(props.target || 0, props.current || 0);
  const fifth = (target / 5).toPrecision(1)*1;
  const step = (fifth/5).toPrecision(1)*5;
  const marks = [];
  let currentStep = 0;
  while(currentStep < (target - (step * 0.5))) {
    marks.push({ value: currentStep, label: currentStep });
    currentStep += step;
  }
  marks.push({ value: target, label: target });

  return (
    <Container>
      <Slider
        value={props.current}
        max={target}
        marks={marks}
        valueLabelDisplay="on"
        disabled
        sx={{
          '&.Mui-disabled': {
            color: '#55aa77'
          },
          '& .MuiSlider-valueLabel': {
            width: 40,
            height: 40,
            backgroundColor: '#55aa77',
            borderRadius: '50% 50% 50% 0',
            transformOrigin: 'bottom left',
            transform: 'translate(50%, -100%) rotate(-45deg) scale(0)',
            '&:before': { display: 'none' },
            '&.MuiSlider-valueLabelOpen': {
              transform: 'translate(50%, -100%) rotate(-45deg) scale(1)'
            },
            '& > *': {
              transform: 'rotate(45deg)'
            }
          }
        }}
      />
    </Container>
  );
}