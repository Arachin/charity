import React from 'react';
import { AppBar, Avatar, Box, Button, IconButton, Toolbar, Typography } from '@mui/material';
import HomeIcon from '@mui/icons-material/Home';
import { Link } from 'react-router-dom';
import { Auth } from "aws-amplify";
import { useNavigate } from "react-router-dom";
import { useAppContext } from "../../util/context";
import ModeSwitch from '../modeSwitch/ModeSwitch';

export default function Header() {
  const { user, logout, isDark, setIsDark } = useAppContext();
  const navigate = useNavigate();

  async function handleLogout() {
    await Auth.signOut();
    logout();
    navigate("/login");
  }

  function handleDarkModeSwitch(event) {
    setIsDark(event.target.checked);
  }

  return (
    <Box height={62}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ marginRight: 2 }}
            component={Link} to='/'
          >
            <HomeIcon />
          </IconButton>
          <Typography variant="h6" color="white" component={Link} to='/' sx={{ textDecoration: 'none' }}>
            London to Tehran
          </Typography>
          <Box sx={{ marginLeft: 'auto', marginRight: 'auto' }} />
          <ModeSwitch checked={isDark} onChange={handleDarkModeSwitch}/>
          { user ?
            <>
              <Typography variant="h6" color="white" component={Link} to={`/user/${user.id}`} sx={{
                textDecoration: 'none',
                display: { xs: 'none', sm: 'block' }
              }}>
                Hi { user.name || user.id }
              </Typography>
              <Avatar
                alt={user.name || user.id}
                src={user.avatar || process.env.PUBLIC_URL + '/defaultAvatar.png'}
                component={Link} to={`/user/${user.id}`}
                sx={{ marginLeft: 1, marginRight: 1 }} />
              <Button color='inherit' onClick={handleLogout}>Logout</Button>
            </>
          :
            <>
              <Button color='inherit' href='/login'>Login</Button>
              <Button color='inherit' href='/register'>Register</Button>
            </>
          }
        </Toolbar>
      </AppBar>
    </Box>
  );
}