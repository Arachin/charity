import React from 'react';
import { Grid, Paper } from '@mui/material';

export default function TwoPane(props) {
    return (
      <Grid container component="main" sx={{ height: 'calc(100vh - 62px)' }}>
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: 'url(https://source.unsplash.com/random?running)',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          {props.children}
        </Grid>
      </Grid>
    );
  }