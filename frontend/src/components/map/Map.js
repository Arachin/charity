import { Card, CardMedia } from '@mui/material';
import React from 'react';
import ProgressBar from '../../components/progressBar/ProgressBar';

export default function Map(props) {
  return (
    <Card sx={{ maxWidth: 800 }}>
      <CardMedia
        component="img"
        image={ process.env.PUBLIC_URL + '/map.png' }
      />
      <ProgressBar current={props.current} target={props.target} />
    </Card>
  );
}