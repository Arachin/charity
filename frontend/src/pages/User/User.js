import { Button, Container, Stack, Card, CircularProgress } from '@mui/material';
import { useParams } from 'react-router';
import ProgressBar from '../../components/progressBar/ProgressBar';
import Feed from '../../components/feed/Feed';
import UserCard from '../../components/userCard/UserCard';
import { useEffect, useState } from 'react';
import { API } from "aws-amplify";

export default function UserPage() {
  const { id } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [userData, setUserData] = useState(null);

  function updatePage() {
    API.get('api', `/user/${id}`)
      .then((result) => {
        setUserData(result);
        setIsLoading(false);
      })
      .catch(console.log);
  }
  
  useEffect(updatePage, [id]);

  return (
    <Container>
      { isLoading ? <CircularProgress /> : 
        <Stack spacing={3} marginY={3} alignItems='center'>
          <UserCard {...userData} updatePage={updatePage} />
          <Card sx={{ width: 530, overflow:'visible' }}>
            <ProgressBar current={userData.current} target={userData.target} />
          </Card>
          <Feed owner={id} feeds={userData.feeds} updatePage={updatePage} />
          <Button variant="text" target="_top" href={`mailto:help@londontotehran.com`}>
            Contact us
          </Button>
        </Stack>
      }
    </Container>
  );
}