import { Button, Container, Grid, Typography } from '@mui/material';

export default function CampaignWeGoToThemPage() {
  return (
    <Container sx={{ display: 'flex', padding: '50px' }}>
      <Grid container>
        <Grid item xs={12}>
          <Typography variant='h2' color='secondary' sx={{ fontWeight: 700 }}>WHAT IS THE LONDON TO TEHRAN CHALLENGE</Typography>
        </Grid>
        <Grid item xs={12}>
          <p>If they can't come to us, we'll go to them! This October, we are inviting everyone to walk, run, scoot, skate and cycle the 3,209 miles from London to Tehran in support of Morad Tahbaz and Mehran Raoof.</p>
          <p>You can take on the challenge as an individual or as part of a local team but all of us involved across the UK will be taking on the challenge together. Every mile you track will be added to the overall challenge total and help us to reach this goal!</p>
          <p>We have 31 days to together total up 3,209 miles all while calling for Morad and Mehran's immediate, unconditional release and return to their loved ones.</p>
        </Grid>
        <Grid item xs={12}>
          <Typography variant='h2' color='primary' sx={{ fontWeight: 700, marginTop: 3 }}>WHY TAKE ON THE CHALLENGE</Typography>
        </Grid>
        <Grid item xs={12}>
          <p>The 1st October will be Morad's 67th birthday and the 5th that he has spent away from his children, either in prison or under a travel ban in Iran.</p>
          <p>In March 2022, Nazanin Zaghari-Ratcliffe and Anoosheh Ashoori were finally released from prison in Tehran and flown back to the UK to be with their loved ones. Morad and Mehran were excluded from this deal.</p>
          <p>Amnesty International UK have detailed compelling evidence that Iran's detention of Nazanin Zaghari-Ratcliffe amounted to an act of hostage-taking, which is a crime under international law, and their concerns for other held dual and foreign nationals whose arbitrary detention may also amount to hostage-taking. The Iranian authorities are currently unjustly detaining Swedish-Iranian Ahmadreza Djalali; Austrian-Iranians Kamran Ghaderi and Massud Mossaheb; German-Iranians Nahid Taghavi and Jamshid Sharmahd; and the two British-Iranians: Mehran Raoof and Morad Tahbaz.</p>
          <p>In March 2022, an 82-year-old Australian-Iranian national, Shokrollah Jebeli (photographed), died in custody after the authorities deliberately denied him adequate specialized medical care and withheld his medication for his multiple serious health conditions.</p>
          <p>We must come together across the UK and insist that the UK Government ensure that all unjustly detained British-Iranian dual nationals are released while working with the international community to ensure this hostage taking diplomacy does not continue to destroy the lives of innocent people.</p>
        </Grid>
        <Grid item xs={12}>
          <Typography variant='h2' color='secondary' sx={{ fontWeight: 700, marginTop: 3 }}>HOW TO GET INVOLVED</Typography>
        </Grid>
        <Grid item xs={12} sx={{ marginBottom: 2 }}>
          <p><a href='/login'>Sign up</a> and join the community across the UK who will be taking on the London to Tehran Challenge together throughout October.</p>
          <p>You can take part as an individual or as a local group but every single one of us are all part of one big team taking on this challenge together. We are always stronger when we're united and this challenge is no different! You can set your own individual distance challenge on your personal page and each mile you track will be added to the overall total.</p>
          <p>You may want to organise group or community events (walks/cycles/etc) to build your distance together or you may prefer to set a solo-challenge. Whatever works best for you, do it! The most important thing is that we are all taking on this challenge and speaking with one voice throughout October.</p>
          <p>Any questions please reach out to us at <a href='mailto:help@londontotehran.com'>amnesty.cardiff@btinternet.com</a></p>
        </Grid>
        <Grid item xs={4} sx={{
          border: 10,
          borderColor: 'secondary.main',
          backgroundImage: `url(${process.env.PUBLIC_URL}/wegotothem/bringmydadhome.jpeg)`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
        }} />
        <Grid item xs={8} sx={{ padding: 2, bgcolor: 'secondary.main', color: 'white' }}>
          <Typography variant='h5' sx={{ fontWeight: 700, padding: 1 }}>"WITHOUT A SHADOW OF A DOUBT, GOVERNMENT OFFICIALS LED US TO BELIEVE THAT OUR FATHER WOULD BE INCLUDED IN ANY DEAL. MY FATHER IS STILL WITHERING AWAY IN PRISON. THE MANNER IN WHICH THIS HAS BEEN HANDLED HAS MADE US FEEL COMPLETELY BETRAYED - WE'VE BEEN MISINFORMED, MISLEAD AND ABANDONED."</Typography>
          <Typography variant='subtitle'>- ROXANNE TAHBAZ, MORAD AND VIDA'S DAUGHTER</Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant='h2' color='primary' sx={{ fontWeight: 700, marginTop: 4 }}>FURTHER ACTIONS</Typography>
        </Grid>
        <Grid item xs={12} sx={{ marginBottom: 2 }}>
          <p>The aim of this challenge is to show how many of us in the UK care deeply about the situation in which Morad, Mehran and their loved ones find themselves. We can do this by using social media to share photographs of us going the extra mile using hashtags:<br />#FreeMorad #FreeMehran #BringThemHome #NoOneLeftBehind #LondonToTehran</p>
          <p>If you are using Twitter you can also tag your local MP, the Prime Minister and the Foreign Secretary @FCDOGovUK and @AmnestyUK. We would encourage people to make signs and banners with messages of support and solidarity for Morad and Mehran.</p>
          <p>We will be asking social media users to be particularly active on the 1st October, Morad's birthday, and the 16th October, which marks 2 years since Mehran's arrest.</p>
          <p>Encourage people you know who want to support this to sign Amnesty UK's ongoing online action calling for the UK Government to meet with the Tahbaz family and secure Moard's immediate release: <a href='https://www.amnesty.org.uk/actions/FreeMorad'>https://www.amnesty.org.uk/actions/FreeMorad</a></p>
          <p>The Cardiff Amnesty group will share information about any additional petitions or actions throughout October so if you would like to be kept up to date on this please do email <a href='mailto:help@londontotehran.com'>amnesty.cardiff@btinternet.com</a></p>
          <p>You could invite your local MP to join you for one of your challenge outings.</p>
        </Grid>
        <Grid item xs={8} sx={{ padding: 2, bgcolor: 'secondary.main', color: 'white' }}>
          <Typography variant='h5' sx={{ fontWeight: 700, padding: 1 }}>"I'VE SAID NUMEROUS TIMES SINCE I GOT BACK THAT I CAN'T FULLY SAVOUR MY FREEDOM WHILE MORAD, MEHRAN AND OTHERS ARE LANGUISHING BEHIND BARS AFTER ENDURING THE SAME TRAVESTY OF JUSTICE I WENT THROUGH. I'M GOING TO KEEP SPEAKING ABOUT THIS UNTIL MORAD AND THE OTHERS ARE BACK - JUST LIKE MY INCREDIBLE FAMILY DID FOR ME."</Typography>
          <Typography variant='subtitle'>- ANOOSHEH ASHOORI</Typography>
        </Grid>
        <Grid item xs={4} sx={{
          border: 10,
          borderColor: 'secondary.main',
          backgroundImage: `url(${process.env.PUBLIC_URL}/wegotothem/anoosheh.jpg)`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
        }} />
        <Grid item xs={12}>
          <Typography variant='h2' color='secondary' sx={{ fontWeight: 700, marginTop: 4 }}>FUNDRAISING</Typography>
        </Grid>
        <Grid item xs={12} sx={{ marginBottom: 1 }}>
          <p>This challenge will continue to shine a spotlight on Morad and Mehran's imprisonment so taking part for one walk or a short cycle is all hugely beneficial. If you would like to use your own distance challenge as an opportunity to fundraise for Amnesty UK then they have set up a JustGiving page specifically for this challenge which you can link to on your challenge profile: <a href='https://www.justgiving.com/campaign/AmnestyLondonTehran'>https://www.justgiving.com/campaign/AmnestyLondonTehran</a></p>
          <p>Fundraising is not a central element of this challenge as it is about coming together as a concerned community to call for action from our government so there is no obligation to make your personal challenge a fundraiser.</p>
        </Grid>
        <Grid item xs={12} sx={{ padding: 2, bgcolor: '#fff200', color: 'white', marginBottom: 3 }}>
          <Typography variant='h4' align='center' color='black' sx={{ fontWeight: 700 }}>THANK YOU FOR TAKING THE LONDON TO TEHRAN CHALLENGE!</Typography>
        </Grid>
        <Grid item xs={12} sx={{ display: 'flex' }}>
          <Button variant="text" target="_top" href={`mailto:help@londontotehran.com`} sx={{ marginLeft: 'auto', marginRight: 'auto' }}>
            Contact us
          </Button>
        </Grid>
      </Grid>
    </Container>
  );
}