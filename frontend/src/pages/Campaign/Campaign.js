import { Button, CircularProgress, Container, Stack } from '@mui/material';
import { useParams } from 'react-router';
import CampaignCard from '../../components/campaignCard/CampaignCard';
import Feed from '../../components/feed/Feed';
import Map from '../../components/map/Map';
import { useEffect, useState } from 'react';
import { API } from "aws-amplify";

export default function CampaignPage(props) {
  const { id } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [campaignData, setCampaignData] = useState(null);

  function updatePage() {
    API.get('api', `/campaign/${props.campaignId || id}`)
      .then((result) => {
        setCampaignData(result);
        setIsLoading(false);
      })
      .catch(console.log);
  }
  
  useEffect(updatePage, [props.campaignId, id]);

  return (
    <Container>
      { isLoading ? <CircularProgress /> : 
        <Stack spacing={3} marginY={3} alignItems='center'>
          <CampaignCard {...campaignData} />
          <Map {...campaignData} />
          <Feed updatePage={updatePage} owner={props.campaignId || id} showUserDetails feeds={campaignData.feeds} />
          <Button variant="text" target="_top" href={`mailto:help@londontotehran.com`}>
            Contact us
          </Button>
        </Stack>
      }
    </Container>
  );
}