import { Button, Container, Grid, Typography } from '@mui/material';

export default function CampaignAboutPage() {
  return (
    <Container sx={{ display: 'flex', padding: '50px' }}>
      <Grid container>
        <Grid item xs={6} sx={{ height: '300px', display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
          <Typography variant='h3' align='center'>Morad Tahbaz</Typography>
          <Typography variant='h5' align='center'>Detained since January 2018</Typography>
        </Grid>
        <Grid item xs={6} sx={{
          backgroundImage: `url(${process.env.PUBLIC_URL}/about/morad.jpeg)`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
        }} />
        <Grid item xs={12}>
          <p>Morad Tahbaz is a father of three who co-founded a conservation charity to support Iranian wildlife, the Persian Wildlife Heritage Foundation. It was during a 2018 conservation visit to track endangered animals that he and colleagues were arrested. Morad has since faced a grossly unfair trial which resulted in him being charged with "cooperating with a hostile state against the Islamic republic” and sentenced to 10 years in prison. In late July this year he was once again furloughed from prison but remains under a travel ban, unable to leave Iran, with the threat of return to the notorious Evin prison ever-present.</p>
          <p>Morad is also unwell. He has cancer and his family are increasingly worried for his welfare. He has lost a significant amount of weight and is denied adequate medical care and treatment.</p>          
          <p>When the UK government made a deal to free British nationals Nazanin Zaghari-Ratcliffe and Anoosheh Ashoori, Morad's family were promised that he would be included too. But instead, Morad was left behind. He was granted furlough from prison, but was imprisoned again within 48 hours.</p>         
          <p>His wife, Vida, was with him in Iran and has remained under a travel ban, unable to leave Iran.</p>
          <p>Sign Amnesty International's urgent letter calling for Morad's immediate and unconditional release at <a href='https://amnesty.org.uk/actions/FreeMorad'>amnesty.org.uk/actions/FreeMorad</a>.</p>
          <p>Follow @FreeMorad1 on twitter for updates and to support the Tahbaz family's campaign.</p>
        </Grid>
        <Grid item xs={12} sx={{ height: '25px' }}></Grid>
        <Grid item xs={6} sx={{
          backgroundImage: `url(${process.env.PUBLIC_URL}/about/mehran.jpeg)`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
        }} />
        <Grid item xs={6} sx={{ height: '300px', display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
          <Typography variant='h3' align='center'>Mehran Raoof</Typography>
          <Typography variant='h5' align='center'>Detained since October 2020</Typography>
        </Grid>
        <Grid item xs={12}>
          <p>Mehran Raoof is a labour rights activist who was arrested alongside fellow campaigners in October 2020. He was held in solitary confinement in Evin prison for a month before being moved to section 2A, which is under the control of the Iranian Revolutionary Guard. Amnesty International has reported on the Revolutionary Guard's “pattern of subjecting detainees to torture to extract forced ‘confessions' which are later used to issue convictions in unfair trials” and made calls for Mehran's immediate release.</p>
          <p>Before facing trial, Mehran was not able to contact his immediate family or to secure legal counsel for himself. His friends arranged an independent lawyer but the authorities refused to grant the lawyer access to his case file until trial.</p>
          <p>In August 2021, a Revolutionary Court convicted him of national security related charges and sentenced him to 10 years and eight months in prison, following a grossly unfair trial. Amnesty International considers Mehran Raoof a prisoner of conscience, detained solely for peacefully advocating for workers' rights in Iran and supporting trade unions.</p>
          <p>To support Mehran, write to the Head of the Judiciary, Gholamhossein Mohseni Ejei c/o Embassy of Iran to the European Union Avenue Franklin Roosevelt No. 15, 1050 Bruxelles, Belgium.</p>
        </Grid>
        <Grid item xs={12} sx={{ display: 'flex' }}>
          <Button variant="text" target="_top" href={`mailto:help@londontotehran.com`} sx={{ marginLeft: 'auto', marginRight: 'auto' }}>
            Contact us
          </Button>
        </Grid>
      </Grid>
    </Container>
  );
}