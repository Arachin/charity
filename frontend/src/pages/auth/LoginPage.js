import React, { useState } from 'react';
import { Alert, Avatar, Box, Button, Grid, Link, TextField, Typography } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import TwoPane from '../../components/twoPane/TwoPane';
import { Auth } from "aws-amplify";
import { useNavigate } from "react-router-dom";
import { useAppContext } from "../../util/context";

export default function LoginPage() {
  const [emailOrUsername, setEmailOrUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [validationError, setValidationError] = useState(null);
  const { loadUser } = useAppContext();
  const navigate = useNavigate();

  async function handleSubmit(event) {
    event.preventDefault();
    setValidationError(null);
    setIsLoading(true);
    try {
      await Auth.signIn(emailOrUsername, password);
      const username = await loadUser();
      navigate(`/user/${username}`);
    } catch (error) {
      console.log(error);
      setValidationError(error.toString().split('Exception:')[1]);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <TwoPane>
      <Box
        sx={{
          my: 8,
          mx: 4,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign In
        </Typography>
        <Box component="form" noValidate disabled={isLoading} onSubmit={handleSubmit} sx={{ mt: 1 }}>
          <TextField
            value={emailOrUsername}
            onInput={ event => setEmailOrUsername(event.target.value) }
            label='Email or Username'
            margin="normal"
            required
            fullWidth
            autoComplete="email"
            autoFocus
          />
          <TextField
            value={password}
            onInput={ event => setPassword(event.target.value) }
            label='Password'
            margin="normal"
            required
            fullWidth
            autoComplete="current-password"
            type="password"
          />
          { validationError &&
            <Alert severity="info">{ validationError }</Alert>
          }
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
          <Grid container flexDirection='column' alignItems='flex-end'>
            <Grid item>
              <Link href="/register" variant="body2">
                Don't have an account? Sign Up
              </Link>
            </Grid>
            <Grid item>
              <Link href="/forgot" variant="body2">
                Forgot your password?
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </TwoPane>
  );
}