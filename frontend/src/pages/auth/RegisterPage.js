import React, { useState } from 'react';
import { Alert, Avatar, Box, Button, Grid, Link, TextField, Typography } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import TwoPane from '../../components/twoPane/TwoPane';
import { Auth } from 'aws-amplify';
import { useNavigate } from 'react-router-dom';
import { useAppContext } from '../../util/context';

export default function RegisterPage() {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [validationError, setValidationError] = useState(null);
  const { loadUser } = useAppContext();
  const navigate = useNavigate();

  async function handleSubmit(event) {
    event.preventDefault();
    if (password !== confirmPassword) {
      setValidationError('Passwords do not match');
      return;
    }
    setValidationError(null);
    setIsLoading(true);
    try {
      await Auth.signUp({
        username,
        password,
        attributes: { email }
      });
      await Auth.signIn(username, password);
      loadUser();
      navigate(`/user/${username}`);
    } catch (error) {
      console.log(error);
      if (error.toString().includes('password')) {
        setValidationError('Password did not conform with policy: Password not long enough');
      } else {
        setValidationError(error.toString().split('Exception:')[1]);
      }
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <TwoPane>
      <Box
        sx={{
          my: 8,
          mx: 4,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign Up
        </Typography>
        <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
          <TextField
            value={email}
            onInput={ event => setEmail(event.target.value) }
            label='Email'
            margin="normal"
            required
            fullWidth
          />
          <TextField
            value={username}
            onInput={ event => setUsername(event.target.value.toLowerCase()) }
            inputProps={{ style: { textTransform: 'lowercase' } }}
            label='Username'
            margin="normal"
            required
            fullWidth
            autoFocus
          />
          <TextField
            value={password}
            onInput={ event => setPassword(event.target.value) }
            label='Password'
            margin="normal"
            required
            fullWidth
            type="password"
          />
          <TextField
            value={confirmPassword}
            onInput={ event => setConfirmPassword(event.target.value) }
            label='Confirm Password'
            margin="normal"
            required
            fullWidth
            autoComplete="nope"
            type="password"
          />
          { validationError &&
            <Alert severity="info">{ validationError }</Alert>
          }
          <Button
            type="submit"
            disabled={isLoading}
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign Up
          </Button>
          <Typography variant="caption">We only use cookies to keep you logged in, and to control the lights :)</Typography>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="/login" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </TwoPane>
  );
}