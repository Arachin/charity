import React, { useState } from 'react';
import { Alert, Avatar, Box, Button, TextField, Typography } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import TwoPane from '../../components/twoPane/TwoPane';
import { Auth } from 'aws-amplify';
import { useNavigate } from "react-router-dom";

export default function ForgotPassword() {
  const [emailOrUsername, setEmailOrUsername] = useState('');
  const [code, setCode] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [validationError, setValidationError] = useState(null);
  const [isSent, setIsSent] = useState(false);
  const navigate = useNavigate();

  async function handleSubmitRequest(event) {
    event.preventDefault();
    setValidationError(null);
    setIsLoading(true);
    try {
      await Auth.forgotPassword(emailOrUsername);
      setIsSent(true);
    } catch (error) {
      console.log(error);
      setValidationError(error.toString().split('Exception:')[1]);
    } finally {
      setIsLoading(false);
    }
  };

  async function handleSubmitNewPassword(event) {
    event.preventDefault();
    if (newPassword !== confirmPassword) {
      setValidationError('Passwords do not match');
      return;
    }
    setValidationError(null);
    setIsLoading(true);
    try {
      await Auth.forgotPasswordSubmit(emailOrUsername, code, newPassword);
      navigate(`/login`);
    } catch (error) {
      console.log(error);
      if (error.toString().includes('password')) {
        setValidationError('Password did not conform with policy: Password not long enough');
      } else {
        setValidationError(error.toString().split('Exception:')[1]);
      }
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <TwoPane>
      <Box
        sx={{
          my: 8,
          mx: 4,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Password Recovery
        </Typography>
        { isSent ? 
          <Box component="form" onSubmit={handleSubmitNewPassword} sx={{ mt: 1 }}>
            <Typography>
              Please check your email for a recovery code.
            </Typography>
            <TextField
              value={code}
              onInput={ event => setCode(event.target.value) }
              label='Code'
              margin="normal"
              required
              fullWidth
            />
            <TextField
              value={newPassword}
              onInput={ event => setNewPassword(event.target.value) }
              label='New Password'
              margin="normal"
              required
              fullWidth
              type="password"
            />
            <TextField
              value={confirmPassword}
              onInput={ event => setConfirmPassword(event.target.value) }
              label='Confirm Password'
              margin="normal"
              required
              fullWidth
              autoComplete="nope"
              type="password"
            />
            { validationError &&
              <Alert severity="info">{ validationError }</Alert>
            }
            <Button
              type="submit"
              disabled={isLoading}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Change Password
            </Button>
          </Box>
        :
          <Box component="form" onSubmit={handleSubmitRequest} sx={{ mt: 1 }}>
            <TextField
              value={emailOrUsername}
              onInput={ event => setEmailOrUsername(event.target.value) }
              label='Email or Username'
              margin="normal"
              required
              fullWidth
            />
            { validationError &&
              <Alert severity="info">{ validationError }</Alert>
            }
            <Button
              type="submit"
              disabled={isLoading}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Send Recovery Email
            </Button>
          </Box>
        }
      </Box>
    </TwoPane>
  );
}