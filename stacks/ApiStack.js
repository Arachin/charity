import * as sst from "@serverless-stack/resources";
import { HttpUserPoolAuthorizer } from "@aws-cdk/aws-apigatewayv2-authorizers-alpha";

export default class ApiStack extends sst.Stack {
  constructor(scope, id, props) {
    super(scope, id, props);

    const { bucket, tables } = props;

    this.auth = new sst.Auth(this, "Auth", {
      cognito: {
        userPool: {
          signInAliases: {
            username: true,
            preferredUsername: true,
            email: true
          },
          autoVerify: {
            email: false
          },
          passwordPolicy: {
            requireSymbols: false,
            requireUppercase: false
          }
        },
        defaultFunctionProps: {
          environment: {
            ENV_NAME: `${scope.stage}-${scope.name}`
          }
        },
        triggers: {
          preSignUp: "handlers/admin/confirmSignUp.main",
          postConfirmation: "handlers/user/post.main"
        }
      }
    });
    this.auth.attachPermissionsForTriggers([...tables]);

    this.api = new sst.Api(this, "Api", {
      customDomain: scope.stage === 'prod' ? 'api.londontotehran.com' : `${scope.stage}-${scope.name}-api.deadbandgames.com`,
      defaultAuthorizationType: sst.ApiAuthorizationType.JWT,
      defaultAuthorizer: new HttpUserPoolAuthorizer("Authorizer", this.auth.cognitoUserPool, {
        userPoolClients: [this.auth.cognitoUserPoolClient]
      }),
      defaultFunctionProps: {
        environment: {
          ENV_NAME: `${scope.stage}-${scope.name}`,
          BUCKET_NAME: bucket.bucketName
        }
      },
      routes: {
        "GET    /campaign/{campaign}": {
          function: "handlers/campaign/get.main",
          authorizationType: sst.ApiAuthorizationType.NONE
        },
        "PUT    /campaign/{campaign}": "handlers/campaign/update.main",
        "GET    /user/{user}": {
          function: "handlers/user/get.main",
          authorizationType: sst.ApiAuthorizationType.NONE
        },
        "PUT    /user/{user}": "handlers/user/update.main",
        "POST   /feed": "handlers/feed/post.main",
        "PUT    /feed/{timestamp}": "handlers/feed/update.main",
        "DELETE /feed/{timestamp}": "handlers/feed/delete.main",
        "POST   /feed/{timestamp}/{owner}/like": "handlers/feed/like/post.main",
        "DELETE /feed/{timestamp}/{owner}/like": "handlers/feed/like/delete.main",
        "POST   /upload": "handlers/upload/post.main"
      }
    });
    this.api.attachPermissions([bucket, ...tables]);
    this.auth.attachPermissionsForAuthUsers([this.api]);

    this.addOutputs({
      UserPoolId: this.auth.cognitoUserPool.userPoolId,
      UserPoolClientId: this.auth.cognitoUserPoolClient.userPoolClientId,
      ApiEndpoint: this.api.customDomainUrl || this.api.url
    });
  }
}