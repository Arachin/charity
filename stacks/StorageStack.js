import * as sst from "@serverless-stack/resources";

export default class StorageStack extends sst.Stack {
  constructor(scope, id, props) {
    super(scope, id, props);

    this.bucket = new sst.Bucket(this, "Uploads", {
      s3Bucket: {
        bucketName: `${scope.stage}-${scope.name}`,
        cors: [ // TODO: Do we need this with the pre-signed url?
          {
            allowedOrigins: ["*"],
            allowedHeaders: ["*"],
            allowedMethods: ["PUT"]
          }
        ]
      }
    });

    this.tables = [];
    this.tables.push(new sst.Table(this, "Users", {
      fields: {
        id: 'S',
        email: 'S',
        name: 'S',
        avatar: 'S',
        bio: 'S',
        donate: 'S',
        target: 'N'
      },
      primaryIndex: { partitionKey: 'id' }
    }));
    this.tables.push(new sst.Table(this, "Campaigns", {
      fields: {
        id: 'S',
        name: 'S',
        bio: 'S',
        target: 'N',
        users: 'SS'
      },
      primaryIndex: { partitionKey: 'id' }
    }));
    this.tables.push(new sst.Table(this, "Feeds", {
      fields: {
        owner: 'S',
        timestamp: 'S',
        name: 'S',
        avatar: 'S',
        message: 'S',
        distance: 'N',
        image: 'S',
        likes: 'SS'
      },
      primaryIndex: { partitionKey: 'owner', sortKey: "timestamp" }
    }));
  }
}