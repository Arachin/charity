import handler from '../utils/handler';
import { postUser, joinCampaign } from '../utils/dynamo';

export const main = handler(async (event) => {
  await postUser(event.userName, event.request.userAttributes.email);
  await joinCampaign('london-to-tehran', event.userName);
}, true);