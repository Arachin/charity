import Joi from 'joi';
import handler from '../utils/handler';
import { updateUser } from '../utils/dynamo';

export const main = handler(async (event) => {
  const id = event.pathParameters.user;
  const body = JSON.parse(event.body);
  body.target = parseFloat(body.target) || 0;
  const schema = Joi.object({
    name: Joi.string().min(3).max(20).required(),
    avatar: Joi.string().uri().empty(''),
    bio: Joi.string().max(500).empty(''),
    donate: Joi.string().uri().empty(''),
    target: Joi.number().integer().min(1).empty(0)
  });
  const validationError = schema.validate(body).error;
  if (validationError) {
    throw {
      statusCode: 400,
      message: validationError.message
    };
  }
  const userId = event.requestContext.authorizer.jwt.claims['cognito:username'];
  if (id !== userId) {
    throw {
      statusCode: 403,
      message: "Forbidden"
    };
  }
  if (body.target) {
    body.target = parseFloat(body.target);
  }
  await updateUser(id, body.name, body.avatar, body.bio, body.donate, body.target);
});
