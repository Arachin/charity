import handler from "../utils/handler";
import { getUser, getFeedsByOwner } from "../utils/dynamo";

export const main = handler(async (event) => {
  const userId = event.pathParameters.user;

  const userData = await getUser(userId);
  if (!userData) {
    throw { statusCode: 404 };
  }
  if (!event.queryStringParameters?.simple) {
    userData.feeds = await getFeedsByOwner(userId);
    userData.current = userData.feeds.reduce((counter, feed) => {
      return counter + (parseFloat(feed.distance) || 0)
    }, 0);
  }
    
  return userData;
});
