import * as AWS from 'aws-sdk';
const S3 = new AWS.S3();

exports.getPreSignedUrl = async function(key, contentType) {
  const preSignedUrl = await S3.getSignedUrlPromise('putObject', {
    Bucket: process.env.BUCKET_NAME,
    Key: key,
    ContentType: contentType,
    ACL: 'public-read'
  });
  return {
    preSignedUrl,
    targetUrl: `https://${process.env.BUCKET_NAME}.s3.amazonaws.com/${key}`
  }
}