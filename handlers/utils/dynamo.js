import * as AWS from 'aws-sdk';
const client = new AWS.DynamoDB.DocumentClient();

const tablePrefix = process.env.ENV_NAME;

function sortByTimestamp(feeds) {
  return feeds.sort((a, b) => {
    return a.timestamp > b.timestamp ? -1 : 1;
  })
}

function formatItem(item) {
  Object.keys(item).forEach(key => {
    if (item[key] === undefined) {
      delete item[key];
    }
  });
  return item;
}

function formatResponse(item) {
  Object.keys(item).forEach(key => {
    if (item[key].values) {
      item[key] = item[key].values;
    }
  });
  return item;
}

async function get(table, keyObject) {
  console.log('get', table, keyObject);
  const response = await client.get({
    TableName: `${tablePrefix}-${table}`,
    Key: keyObject
  }).promise();
  return formatResponse(response.Item);
}

async function query(table, hashKey, hashValue) {
  console.log('query', table, `${hashKey}:${hashValue}`);
  const response = await client.query({
    TableName: `${tablePrefix}-${table}`,
    KeyConditionExpression: `#hashKey = :hashValue`,
    ExpressionAttributeNames: { '#hashKey': hashKey },
    ExpressionAttributeValues: { ':hashValue': hashValue }
  }).promise();
  return response.Items.map(formatResponse);
}

async function scan(table) {
  console.log('scan', table);
  const response = await client.scan({
    TableName: `${tablePrefix}-${table}`
  }).promise();
  return response.Items.map(formatResponse);
}

async function put(table, item) {
  item = formatItem(item);
  console.log('put', table, item);
  await client.put({
    TableName: `${tablePrefix}-${table}`,
    Item: item
  }).promise();
}

async function update(table, keyObject, item) {
  item = formatItem(item);
  console.log('update', table, keyObject, item);
  await client.update({
    TableName: `${tablePrefix}-${table}`,
    Key: keyObject,
    UpdateExpression: `SET ${Object.keys(item).map((key) => `#${key} = :${key}`).join(', ')}`,
    ExpressionAttributeNames: Object.fromEntries(Object.keys(item).map((key) => [`#${key}`, key])),
    ExpressionAttributeValues: Object.fromEntries(Object.entries(item).map(([key, value]) => [`:${key}`, value]))
  }).promise();
}

async function pushValue(table, keyObject, arrayName, value) { // TODO: Only add if not exists
  console.log('pushValue', table, keyObject, `${arrayName}:${value}`);
  await client.update({
    TableName: `${tablePrefix}-${table}`,
    Key: keyObject,
    UpdateExpression: `ADD #arrayName :value`,
    ExpressionAttributeNames: { '#arrayName': arrayName },
    ExpressionAttributeValues: { ':value': client.createSet([value]) }
  }).promise();
}

async function popValue(table, keyObject, arrayName, value) { // TODO: Is this tolerant of the element not existing?
  console.log('popValue', table, keyObject, `${arrayName}:${value}`);
  await client.update({
    TableName: `${tablePrefix}-${table}`,
    Key: keyObject,
    UpdateExpression: `DELETE #arrayName :value`,
    ExpressionAttributeNames: { '#arrayName': arrayName },
    ExpressionAttributeValues: { ':value': client.createSet([value]) }
  }).promise();
}

async function del(table, key) {
  console.log('delete', table, key);
  await client.delete({
    TableName: `${tablePrefix}-${table}`,
    Key: key
  }).promise();
}



exports.getUser = async function(id) {
  return await get('Users', { id });
};

exports.postUser = async function(id, email) {
  await put('Users', { id, email });
};

exports.updateUser = async function(id, name, avatar, bio, donate, target) {
  await update('Users', { id }, { name, avatar, bio, donate, target });
  //await update('Feeds', { owner: id }, { name, image: avatar }); // TODO: Loop over feeds for user (by id) and update
};

exports.getCampaign = async function(id) {
  return await get('Campaigns', { id });
};

exports.updateCampaign = async function(id, bio) {
  await update('Campaigns', { id }, { bio });
};

exports.joinCampaign = async function(id, userId) {
  await pushValue('Campaigns', { id }, 'users', userId);
};

exports.getFeedsByOwner = async function(owner) {
  const feeds = await query('Feeds', 'owner', owner);
  return sortByTimestamp(feeds);
};

exports.getAllFeeds = async function() {
  const feeds = await scan('Feeds');
  return sortByTimestamp(feeds);
};

exports.postFeed = async function(owner, message, distance, image) {
  const { name, avatar } = await get('Users', { id: owner });
  await put('Feeds', {
    owner,
    timestamp: new Date().toISOString(),
    name,
    avatar,
    message,
    distance,
    image
  });
};

exports.updateFeed = async function(owner, timestamp, message, distance, image) {
  await update('Feeds', { owner, timestamp }, { message, distance, image });
};

exports.deleteFeed = async function(owner, timestamp) {
  await del('Feeds', { owner, timestamp });
};

exports.likeFeed = async function(owner, timestamp, userId) {
  await pushValue('Feeds', { owner, timestamp }, 'likes', userId);
};

exports.unlikeFeed = async function(owner, timestamp, userId) {
  await popValue('Feeds', { owner, timestamp }, 'likes', userId);
};