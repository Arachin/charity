export default function handler(lambda, isCognito = false) {
  return async function (event, context) {
    let response, statusCode;

    try {
      response = await lambda(event, context);
      if (isCognito) {
        return context.succeed(event);
      }
      statusCode = 200;
    } catch (error) {
      console.error(error);
      if (isCognito) {
        return context.fail(event);
      }
      response = { error: error.message };
      statusCode = error.statusCode || 500;
    }

    return {
      statusCode,
      body: JSON.stringify(response),
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
      }
    };
  };
}