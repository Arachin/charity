import handler from '../utils/handler';

export const main = handler(async (event) => {
  event.response.autoConfirmUser = true;
  event.response.autoVerifyEmail = true;
  return event;
}, true);