import handler from "../utils/handler";
import { deleteFeed } from "../utils/dynamo";

export const main = handler(async (event) => {
  const timestamp = event.pathParameters.timestamp;
  const userId = event.requestContext.authorizer.jwt.claims['cognito:username'];
  await deleteFeed(userId, timestamp);
});
