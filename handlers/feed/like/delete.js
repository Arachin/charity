import handler from "../../utils/handler";
import { unlikeFeed } from "../../utils/dynamo";

export const main = handler(async (event) => {
  const owner = event.pathParameters.owner;
  const timestamp = event.pathParameters.timestamp;
  const userId = event.requestContext.authorizer.jwt.claims['cognito:username'];
  await unlikeFeed(owner, timestamp, userId);
});
