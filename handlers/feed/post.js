import Joi from 'joi';
import handler from '../utils/handler';
import { postFeed } from '../utils/dynamo';

export const main = handler(async (event) => {
  const body = JSON.parse(event.body);
  body.distance = parseFloat(body.distance) || 0;
  console.log(body);
  const schema = Joi.object({
    message: Joi.string().max(500).empty(''),
    distance: Joi.number().min(1).empty(0),
    image: Joi.string().uri().empty('')
  }).or('message', 'distance', 'image');
  const validationError = schema.validate(body).error;
  if (validationError) {
    throw {
      statusCode: 400,
      message: validationError.message
    };
  }
  const userId = event.requestContext.authorizer.jwt.claims['cognito:username'];
  await postFeed(userId, body.message, body.distance, body.image);
});