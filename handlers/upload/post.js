import handler from "../utils/handler";
import { getPreSignedUrl } from "../utils/s3";

const imageTypes = { // TODO: use a library for this
  "image/jpeg": "jpg",
  "image/png": "png",
  "image/gif": "gif"
}

export const main = handler(async (event) => {
  const body = JSON.parse(event.body);
  if (!(body.contentType in imageTypes)) {
    throw {
      statusCode: 400,
      message: "Image type not accepted"
    };
  }
  const userId = event.requestContext.authorizer.jwt.claims['cognito:username'];
  const key = `feedImages/${userId}/${Date.now()}.${imageTypes[body.contentType]}`;
  const { preSignedUrl, targetUrl } = await getPreSignedUrl(key, body.contentType);
  return {
    preSignedUrl,
    targetUrl
  };
});