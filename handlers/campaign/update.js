import Joi from 'joi';
import handler from '../utils/handler';
import { updateCampaign } from '../utils/dynamo';

export const main = handler(async (event) => {
  const id = event.pathParameters.campaign;
  const body = JSON.parse(event.body);
  const schema = Joi.object({
    bio: Joi.string().max(500).empty('')
  });
  const validationError = schema.validate(body).error;
  if (validationError) {
    throw {
      statusCode: 400,
      message: validationError.message
    };
  }
  const userId = event.requestContext.authorizer.jwt.claims['cognito:username'];
  if (id !== userId) {
    throw {
      statusCode: 403,
      message: 'Forbidden'
    };
  }
  await updateCampaign(id, body.bio);
});
