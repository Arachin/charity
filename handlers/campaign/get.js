import handler from "../utils/handler";
import { getCampaign, getAllFeeds } from "../utils/dynamo";

export const main = handler(async (event) => {
  const campaignId = event.pathParameters.campaign;

  const campaignData = await getCampaign(campaignId);
  if (!campaignData) {
    throw { statusCode: 404 };
  }
  campaignData.feeds = await getAllFeeds();
  campaignData.current = campaignData.feeds.reduce((counter, feed) => {
    return counter + (parseFloat(feed.distance) || 0)
  }, 0);

  return campaignData;
});
